from utils import current_day
from data_manipulations.pdframe import data
from plots import Plots
import pandas as pd
from tkinter import *
from form import TkinderForm
from tkinter import messagebox as mb


form = TkinderForm()


def get_plot(radio_btn_status, day, user_id, item_ids):
    plot = Plots(radio_btn_status, user_id, day, item_ids)
    plot.draw_plot()


def get_excel(radio_btn_status, user_id, weekday):
    df = data.get_data(radio_btn_status, user_id)
    df.reset_index(drop=True, inplace=True)
    df = df.rename(columns={'count': 'number_of_orders'})
    your_day_df = df[df['weekday'] == weekday]

    with pd.ExcelWriter(f'{weekday}_recommendation_for_user_{user_id}.xlsx', engine='xlsxwriter') as writer:
        your_day_df.to_excel(writer, sheet_name='Daily')


def get_option_for_all(radio_btn_status, uid, item_ids):
    weekday = current_day()
    get_plot(radio_btn_status, uid, weekday, item_ids)
    get_excel(radio_btn_status, uid, weekday)


def choose_option(radio_btn_status):
    weekday = current_day()
    print("MY BTN STATUS: ", radio_btn_status)

    entry_user_id = form.get_entry_user_id()
    entry_item_ids = form.get_entry_item_ids()

    try:
        uid = int(entry_user_id.get())
        item_ids = list(map(int, entry_item_ids.get().split(',')))
    except (TypeError, ValueError):
        mb.showerror("Error", "Invalid entered data.. Please enter only integer numbers")
        return None
    if radio_btn_status == 0 or radio_btn_status == 2:
        get_excel(radio_btn_status, uid, weekday)
        get_plot(radio_btn_status, uid, weekday, item_ids)
    elif radio_btn_status == 1:
        print("For multiple items")
        get_option_for_all(radio_btn_status, uid, item_ids)


def create_form():
    current_form = form.root_form()
    c = Canvas(current_form, width=600, height=130, bg="lightblue")
    c.pack()
    c.place(x=-2, y=0)

    radio_btn_status = IntVar()
    radio_btn_status.set(0)
    Label(current_form, width=600, text="Choose kind of recommendations:", justify = LEFT, padx = 20, bg="lightblue").pack(side = TOP, anchor=NW)
    Radiobutton(text="Only for one item(1)", variable=radio_btn_status, justify = LEFT, value=0, bg="lightblue").pack(side = TOP, anchor=NW, ipady = 5)
    Radiobutton(text="For multiple items with combinatins of these items(2)", variable=radio_btn_status, justify = LEFT, value=1, bg="lightblue").pack(side = TOP,anchor=NW, ipady = 5)
    Radiobutton(text="For multiple items without combinatins of these items(3)", variable=radio_btn_status, justify = LEFT, value=2, bg="lightblue").pack(side = TOP,anchor=NW, ipady = 5)
    current_form.geometry("600x400")
    btn = form.btn_form()
    btn['command'] = lambda: choose_option(radio_btn_status.get())
    current_form.mainloop()


if __name__ == '__main__':
    create_form()

import matplotlib.pyplot as plt
from data_manipulations.pdframe import data
import pandas as pd

class Plots:
    def __init__(self,radio_btn_status, weekday, user_id, item_ids):
        self._data = data.get_data(radio_btn_status, user_id)
        self._weekday = weekday
        self._user_id = user_id
        self._network_orders_data = data.get_data_recommend_for_item_id(radio_btn_status, item_ids)
        self._rec_for_items = item_ids

    def draw_plot(self):
        self.draw_item_by_day(weekday=self._weekday)

    def join_recommendations(self, weekday):
        result_table = self._data[self._data['weekday'] == weekday]

        recom_for_current_order = self._network_orders_data

        common_rec = self.common_recommendations(result_table['item_id'], recom_for_current_order['item_id'])
        top_3_item_values = self.change_priority_order(common_rec, recom_for_current_order)[:3]

        updated_dataframe_items = self.update_recom_dataframe(top_3_item_values, result_table, recom_for_current_order)

        return updated_dataframe_items

    def draw_item_by_day(self, weekday):
        updated_dataframe_items = self.join_recommendations(weekday)

        item_quantities = updated_dataframe_items['count'].values

        patches, texts, autotexts = plt.pie(updated_dataframe_items['count'], labels=updated_dataframe_items['name'], autopct='%1.2f%%')

        for i, a in enumerate(autotexts):
            a.set_text("{}".format(item_quantities[i]))

        updated_dataframe_items['legend'] = updated_dataframe_items['name'] + '(' +updated_dataframe_items['item_id'].astype(str) + ')'

        plt.legend(patches, updated_dataframe_items['legend'], loc='best')
        plt.title(f'Top 3 recommendations for user {self._user_id} and order with item_id {self._rec_for_items} on {weekday}')
        plt.axis('equal')
        plt.tight_layout()
        plt.show()


    def common_recommendations(self, recom_for_current_day, recom_by_item):
        recom_type_1 = set(recom_for_current_day.values)
        recom_type_2 = set(recom_by_item.values)
        common_rec = set(list(set(recom_type_1) & set(recom_type_2)))

        return common_rec

    def change_priority_order(self, common_rec, recom_for_current_order):
        items_recom = []

        if common_rec:
            diff_recomendations = set(list(set(recom_for_current_order['item_id'].values) - set(common_rec)))
            for item in common_rec:
                items_recom.append(item)

            for item in diff_recomendations:
                items_recom.append(item)
        else:
            items_recom = recom_for_current_order['item_id'].values

        return items_recom

    def update_recom_dataframe(self, top_3_item_values, recom_for_current_day, recom_for_current_order):
        merged_df = recom_for_current_order.merge(recom_for_current_day.drop_duplicates(), on=['item_id','count', 'name'],  how='left', indicator=True)
        recom_df = merged_df.loc[merged_df['item_id'].isin(top_3_item_values)]

        return recom_df

import mysql.connector as connection
from config import conf
from utils import ADMIN_USER_ID, day_of_week
from queries import Query
import pandas as pd
from tkinter import messagebox as mb


class DataPipeline:
    def __init__(self, conf):
        self._connection = connection.connect(**conf)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._connection.close()

    @property
    def connection(self):
        return self._connection

    def get_data_items_for_network_orders(self, radio_btn_status, network_order_ids, item_ids):
        query = Query(network_order_ids=network_order_ids, items_ids=item_ids)
        result_dataframe = pd.read_sql(query.query_network_orders_with_item_id, self.connection)

        return result_dataframe

    def get_data(self,radio_btn_status, user_id):
        query = Query(user_id=user_id)
        result_dataframe = pd.read_sql(query.get_query, self.connection)
        res = self.get_item_id_by_day(radio_btn_status, result_dataframe)
        return res

    def get_data_recommend_for_item_id(self, radio_btn_status, item_ids):
        query = Query(radio_btn_status=radio_btn_status, items_ids=item_ids)
        print(query.radio_btn_status)
        print('SQL: ')
        print(query.query_network_orders_for_item_id)
        network_orders_dataframe = pd.read_sql(query.query_network_orders_for_item_id, self.connection)
        order_networks_ids = network_orders_dataframe['network_order_id'].values
        if order_networks_ids.any():
            network_orders_ids = ', '.join([str(id) for id in order_networks_ids])
            required_order_items_df = self.get_data_items_for_network_orders(radio_btn_status, network_orders_ids, item_ids)
            top_3_recom_df = self.get_top_3_recommend_for_items(radio_btn_status, required_order_items_df)

            return top_3_recom_df
        else:
            mb.showinfo("Information", 'With this combination of item there are not any orders!')

    def get_item_id_by_day(self, radio_btn_status, dataframe):
        dataframe['created_at'] = dataframe['created_at'].dt.weekday.map(day_of_week)
        df = dataframe.filter(['created_at', 'item_id', 'name'], axis=1)

        temp = df.groupby(['created_at', 'item_id', 'name'])['item_id'].count().index.tolist()
        values = df.groupby(['created_at', 'item_id', 'name'])['item_id'].count().values.tolist()
        res_df = pd.DataFrame.from_records(temp, columns=['weekday', 'item_id', 'name'])
        res_df['count'] = values

        res_df = self.get_top_three(radio_btn_status, res_df)
        return res_df

    def get_top_three(self, radio_btn_status, dataframe):
        days_of_week = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
        result_df = pd.DataFrame(columns = ['weekday', 'item_id', 'count'])
        for day in days_of_week:
            result_table = dataframe[dataframe['weekday'] == day]
            res = result_table.nlargest(10, 'count')
            result_df = result_df.append(res)
        return result_df

    def get_top_3_recommend_for_items(sels, radio_btn_status, items_df):
        items_groups_df = items_df.groupby(['item_id', 'name']).size().reset_index(name='count')
        sorted_df = items_groups_df.sort_values(by=['count'], ascending=False)
        top_3_recommendations = sorted_df.iloc[:3]

        return top_3_recommendations


data = DataPipeline(conf)

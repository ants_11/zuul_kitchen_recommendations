import configparser

config = configparser.ConfigParser()
config.read('settings.ini')

conf = {'host': config.get('default', 'host'),
        'port': config.get('default', 'port'),
        'user': config.get('default', 'user'),
        'password': config.get('default', 'password'),
        'database': config.get('default', 'database'),
        }

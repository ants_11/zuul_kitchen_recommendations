from tkinter import *
from tkinter import messagebox as mb


class TkinderForm:
    def __init__(self):
        self.root = Tk()
        self.entry_user_id = Entry(self.root, width = 25)
        self.entry_item_ids = Entry(self.root, width = 25)

        self.root['width'] = 600
        self.root['height'] = 400
        self.root.title('Recommendations ZuulKitchen')

        self.entry_user_id.place(x = 20, y = 170)
        lbl_user_id = Label(self.root, text='Enter user id')
        lbl_user_id.place(x = 260, y = 170)

        self.entry_item_ids.place(x = 20, y = 200)
        lbl_item_ids = Label(self.root, text='Enter item ids(for multiple ids use "," seperator)')
        lbl_item_ids.place(x = 260, y = 200)
        self.btn = Button(self.root, text='Show recommendations')
        self.btn.place(x = 40, y = 230 )

        c = Canvas(self.root, width=600, height=150, bg="lightblue")
        c.pack()
        c.place(x=-2, y=290)
        self.info_one_btn = Button(self.root, text='About recommendations for 1 item(1)', width=60, highlightbackground="lightblue")
        self.info_one_btn.place(x = 20, y = 300 )
        self.info_mult_btn = Button(self.root, text='About recommendations for multiple item with combination(2)',  width=60, highlightbackground="lightblue")
        self.info_mult_btn.place(x = 20, y = 330 )
        self.info_mult_btn_without_com = Button(self.root, text='About recommendations for multiple item without combination(3)',  width=60, highlightbackground="lightblue")
        self.info_mult_btn_without_com.place(x = 20, y = 360 )
        self.info_one_btn['command'] = self.show_info_about_rec_one_item
        self.info_mult_btn['command'] = self.show_info_about_rec_mult_item
        self.info_mult_btn_without_com['command'] = self.show_info_about_rec_few_items


    def root_form(self):
        return self.root

    def btn_form(self):
        return self.btn

    def get_entry_user_id(self):
        return self.entry_user_id

    def get_entry_item_ids(self):
        return self.entry_item_ids

    def get_one_info_btn(self):
        return self.info_one_btn

    def get_mult_info_btn(self):
        return self.info_mult_btn

    def show_info_about_rec_one_item(self):
        info = "This example of recommendations algorithm processing only recommendations for one item and recommendations for user by current weekday. The output is top 3 recommended items"
        mb.showinfo("Information", info)

    def show_info_about_rec_mult_item(self):
        info = "This example of recommendations algorithm processing multiple items and searching only orders with combination of provided items and with recommendations for user by current weekday. The output is top 3 recommended items"
        mb.showinfo("Information", info)

    def show_info_about_rec_few_items(self):
        info = "This example of recommendations algorithm processing multiple items and searching all orders that includes at least one of these items and with recommendations for user by current weekday. The output is top 3 recommended items"
        mb.showinfo("Information", info)

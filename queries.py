from utils import ADMIN_USER_ID


class Query:
    def __init__(self, radio_btn_status=None, user_id=None, items_ids=None or [], network_order_ids=None or []):
        self._user_id = user_id
        self._items_ids = items_ids
        self._list_of_items = self._items_ids[0] if len(self._items_ids) == 1 else str(self._items_ids).strip('[]')
        self._network_order_ids = network_order_ids
        self.radio_btn_status = radio_btn_status

    @property
    def query_network_orders_for_item_id(self):
        print(len(self._items_ids))
        if self.radio_btn_status == 0 or self.radio_btn_status == 2:
            query = f"""
                SELECT item_id, network_order_id
                FROM order_items
                INNER JOIN network_orders ON order_items.network_order_id = network_orders.id
                AND network_orders.user_id != {ADMIN_USER_ID} AND item_id IN ({self._list_of_items})
                """
            return query
        if self.radio_btn_status == 1:
            query = f"""
                SELECT network_order_id FROM (SELECT DISTINCT(item_id), network_order_id FROM order_items
                INNER JOIN network_orders ON order_items.network_order_id = network_orders.id
                WHERE network_orders.user_id != {ADMIN_USER_ID} AND item_id IN({self._list_of_items})) AS temp
                GROUP BY temp.network_order_id HAVING COUNT(*)>1;
                """
            return query

    @property
    def query_network_orders_with_item_id(self):
        query = f"""
                SELECT name, item_id, network_order_id
                FROM order_items
                WHERE network_order_id IN ({self._network_order_ids})
                AND item_id NOT IN ({self._list_of_items})
                ORDER BY network_order_id
                """
        return query

    @property
    def get_query(self):
        query = f"""
                SELECT
                users.id AS user_id,
                order_items.id AS order_item_id,
                order_items.item_id,
                order_items.created_at,
                order_items.status,
                order_items.name
                FROM order_items
                INNER JOIN network_orders ON order_items.network_order_id = network_orders.id
                INNER JOIN users ON network_orders.user_id = users.id WHERE users.id = {self._user_id} AND order_items.status = 'completed';
                """
        return query

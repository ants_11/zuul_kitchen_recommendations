#####
Download and install Python 3.8 from https://www.python.org/downloads/
Tips: easy to install for MacOS via Homebrew: 
```
brew install python@3.8
```
#####Install package manager pip using it(if not installed): 
Use it as reference for installation: https://pip.pypa.io/en/stable/installation/#get-pip-py
#####OR
```
python3 -m pip install --user --upgrade pip
```

#### Install and start virtual environment:
Use it as reference for installation and activation:
https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/

Tips: venv activation for MacOS 
````
source venv/bin/activate
````
#### Install requirements
````
pip install -r requirements.txt
````
#### Change VALUES in settings.ini to your local db values

#### Use recommendations.py as entry point:
````
python3 recommendations.py
````




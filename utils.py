import datetime


ADMIN_USER_ID = 13

day_of_week = {0: "Monday",
               1: "Tuesday",
               2: "Wednesday",
               3: "Thursday",
               4: "Friday",
               5: "Saturday",
               6: "Sunday"
               }


def current_day():
    current_day_num = datetime.date.today().weekday()
    return day_of_week[current_day_num]